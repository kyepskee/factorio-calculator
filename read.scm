(use-modules (srfi srfi-1))
(use-modules (json))
(use-modules (ice-9 textual-ports))
(use-modules (ice-9 pretty-print))

(define (update-non-nil f x)
  (if (nil? x)
      x
      (f x)))

(define (get key alist)
  (let ((res (assoc key alist)))
    (if res
        (cdr res)
        res)))

(define (create-alist . args)
  (cond
   ((nil? args) '())
   ((nil? (cdr args)) (error "create-alist: odd number of args"))
   (else (cons (cons (car args)
                     (cadr args))
               (apply create-alist (drop args 2))))))

(define (alist . args)
  (define (iter args alist)
    (if (nil? args)
        alist
        (iter (cddr args)
              (acons (car args) (cadr args)
                     alist))))
  (iter (drop-right args 1)
        (last args)))

(define (set-normal recipe)
  (let ((normal (get "normal" recipe)))
    (if normal
        (fold (lambda (prop recipe)
                (cons prop recipe))
              recipe
              normal)
        recipe)))

(define (make-item name amount . args)
  (apply create-alist
         'name  name
         'amount amount
         args))
(define (item-name item)
  (get 'name item))

(define (ingredient-to-item ingredient)
  (if (vector? ingredient)
      (make-item (vector-ref ingredient 0)
                 (vector-ref ingredient 1))
      ingredient))
(define eg-expensive-recipe
  '(("normal" ("result" . "pipe") ("ingredients" . #(#("iron-plate" 1)))) ("expensive" ("result" . "pipe") ("ingredients" . #(#("iron-plate" 2))))))

(define (get-recipes-data)
  (define (get-ingredients recipe)
    (let ((ingredients (get "ingredients" recipe)))
      (display ingredients)
      (newline)
      (write recipe)
      (newline)
      (display "--------------")
      (newline)
      (map ingredient-to-item
           (vector->list ingredients))))
  (define (get-results recipe)
    (let ((results (get "results" recipe))
          (result (get "result" recipe))
          (result-count (get "result_count" recipe)))
      (if results
          (begin
            (map (lambda (result)
                   (if (vector? result)
                       (make-item (vector-ref result 0)
                                  (vector-ref result 1))
                       (make-item (get "name" result)
                                  (get "amount" result))))
                 (vector->list results)))
          (list (make-item result
                           (if result-count result-count 1))))))
  (define (transform-recipe recipe name)
    (let ((recipe (set-normal recipe)))
      (alist "results" (get-results recipe)
             "ingredients" (get-ingredients recipe)
             "name" name
             recipe)))
  (let ((data (json->scm (open-input-file "recipes.json"))))
    (map
     (lambda (recipe-list)
       (cons (car recipe-list)
             (transform-recipe (cdr recipe-list) (car recipe-list))))
     data)))

(define recipes-data (get-recipes-data))
(define recipes-list (map cdr recipes-data))


;; (for-each
;;  (lambda (recipe)
;;    (pretty-print recipe))

(define (recipe-results recipe)
  (get "results" recipe))
(define (recipe-ingredients recipe)
  (get "ingredients" recipe))
(define (recipe-name recipe)
  (get "name" recipe))

(define props
  (fold
   (lambda (recipe acc)
     (let ((props (cdr recipe)))
       (fold (lambda (prop acc)
               (let ((prop-name (car prop)))
                 (lset-adjoin equal? acc prop-name)))
             acc
             props)))
   '()
   recipes-data))

(define (search-by-name recipe-name)
  (get recipe-name recipes-data))

(define (search-by-result result-id)
  (filter (lambda (recipe)
            (let* ((results (recipe-results recipe))
                   (names (map item-name results)))
              (member result-id names)))
          recipes-list))

(let ((port (open-output-file "recipes.sexp")))
  (write recipes-data port)
  (close-port port))

(map (lambda (recipe)
       (recipe-results))
     recipe-list)
