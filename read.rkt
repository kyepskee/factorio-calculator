#!r6rs
(import (rnrs)
        (rnrs lists (6))
        (rnrs io ports (6))
        (rnrs files (6)))
;; (require rnrs/lists-6)
;; (require rnrs/io/ports-6)
;; (require rnrs/io/files-6)
; (use-modules (srfi srfi-1))
; (use-modules (json))
; (use-modules (ice-9 textual-ports))
; (use-modules (ice-9 pretty-print))
(define (nil? x)
  (equal? x '()))

(define (update-non-nil f x)
  (if (nil? x)
      x
      (f x)))

(define (get key alist)
  (let ((res (assoc key alist)))
    (if res
        (cdr res)
        res)))

(define (drop lst n)
  (if (> n 0)
      (drop (cdr lst) (- n 1))
      lst))

(define (acons key value alist)
  (cond
    ((nil? alist) (list (cons key value)))
    ((equal? (caar alist) key)
     (cons (cons key value)
           (cdr alist)))
    (else (cons (car alist)
                (acons key value (cdr alist))))))

(define (lset-adjoin x lst)
  (if (member x lst)
      lst
      (cons x lst)))

(define (create-alist . args)
  (cond
   ((nil? args) '())
   ((nil? (cdr args)) (error "create-alist: odd number of args"))
   (else (cons (cons (car args)
                     (cadr args))
               (apply create-alist (drop args 2))))))

(define (make-item name amount . args)
  (apply create-alist
         'name  name
         'amount amount
         args))
(define (item-name item)
  (get 'name item))
(define (item-amount item)
  (get 'amount item))


(define recipes-data
  (let* ((port (open-input-file "recipes.sexp"))
         ; (out (get-string-all port))
         (res (read port)))
    (close-port port)
    res))
(define recipes-list (map cdr recipes-data))

;; (for-each
;;  (lambda (recipe)
;;    (pretty-print recipe))

(define (recipe-results recipe)
  (get "results" recipe))
(define (recipe-ingredients recipe)
  (get "ingredients" recipe))
(define (recipe-name recipe)
  (get "recipe" recipe))

;(define props
;  (fold-left
;   (lambda (recipe acc)
;     (let ((props (cdr recipe)))
;       (fold-left (lambda (prop acc)
;                    (let ((prop-name (car prop)))
;                      (lset-adjoin equal? acc prop-name)))
;             acc
;             props)))
;   '()
;   recipes-data))

(define (search-by-name recipe-name)
  (get recipe-name recipes-data))

(define (search-by-result result-id)
  (filter (lambda (recipe)
            (let* ((results (recipe-results recipe))
                   (names (map item-name results)))
              (member result-id names)))
          recipes-list))

;; (let ((port (open-output-file "recipes-rkt.sexp")))
;;   (write recipes-data port)
;;   (close-port port))